name "libsqlite3"
default_version "3.31.1"

dependency "config_guess"

source url: "https://www.sqlite.org/2020/sqlite-autoconf-3310100.tar.gz",
  sha256: "62284efebc05a76f909c580ffa5c008a7d22a1287285d68b7825a2b6b51949ae"

relative_path "sqlite-autoconf-3310100"

env = {
  "LDFLAGS" => "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include",
  "CFLAGS" => "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include",
  "LD_RUN_PATH" => "#{install_dir}/embedded/lib",
}

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--prefix=#{install_dir}/embedded",
    "--disable-nls"
  ]
  configure(*configure_command, env: env)
  make("-j #{workers}", env: env)
  make("-j #{workers} install", env: env)
end
