name "asdf_mono"

license_file "LICENSE"
license_file "COPYING"
skip_transitive_dependency_licensing true

default_version "6.8.0.123"
source url: "https://download.mono-project.com/sources/mono/mono-#{version}.tar.xz", sha256: "e2e42d36e19f083fc0d82f6c02f7db80611d69767112af353df2f279744a2ac5"

relative_path "mono-#{version}"

dependency "zlib"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--prefix=#{install_dir}/installs/mono/#{version}",
  ]
  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
end
