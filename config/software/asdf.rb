name "asdf"
default_version "0.7.8"
relative_path "asdf-#{version}"

license "MIT"
# license_file "#{project_dir}/LICENSE"

skip_transitive_dependency_licensing true

version("0.7.8") { source md5: "c64fe16441d639669a6bf56a860e0eb4" }

source url: "https://github.com/asdf-vm/asdf/archive/v#{version}.tar.gz"

build do
  block do
    sync project_dir, install_dir
    command "echo 'legacy_version_file = yes' > #{install_dir}/defaults"
    touch "#{install_dir}/installs/.keep"
    touch "#{install_dir}/plugins/.keep"

    env = { 'ASDF_DATA_DIR' => install_dir }
    command "#{install_dir}/bin/asdf plugin-add elixir", env: env
    command "#{install_dir}/bin/asdf plugin-add golang", env: env
    command "#{install_dir}/bin/asdf plugin-add gradle", env: env
    command "#{install_dir}/bin/asdf plugin-add java", env: env
    command "#{install_dir}/bin/asdf plugin-add maven", env: env
    command "#{install_dir}/bin/asdf plugin-add nodejs", env: env
    command "#{install_dir}/bin/asdf plugin-add php", env: env
    command "#{install_dir}/bin/asdf plugin-add python", env: env
    command "#{install_dir}/bin/asdf plugin-add ruby", env: env
    command "#{install_dir}/bin/asdf plugin-add rust", env: env
    command "#{install_dir}/bin/asdf plugin-add sbt", env: env
  end
end
