name "asdf_python"

license "Python-2.0"
license_file "LICENSE"
skip_transitive_dependency_licensing true

default_version "3.8.3"

dependency "libffi"
dependency "ncurses"
dependency "zlib"
dependency "openssl"
dependency "pkg-config"
dependency "bzip2"
dependency "libsqlite3"
dependency "liblzma"
dependency "libyaml"
#dependency "libreadline"

version("3.8.3") { source sha256: "6af6d4d2e010f9655518d0fc6738c7ff7069f10a4d2fbd55509e467f092a8b90" }
version("3.8.1") { source sha256: "c7cfa39a43b994621b245e029769e9126caa2a93571cee2e743b213cceac35fb" }

source url: "https://python.org/ftp/python/#{version}/Python-#{version}.tgz"

relative_path "Python-#{version}"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  configure_command = [
    "--prefix=#{install_dir}/installs/python/#{version}",
    "--with-ssl=#{install_dir}/embedded",
    "--enable-shared",
    "--with-dbmliborder=",
  ]
  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
end
