name "asdf_ruby"

license "BSD-2-Clause"
license_file "BSDL"
license_file "COPYING"
license_file "LEGAL"
skip_transitive_dependency_licensing true

default_version "2.7.1"

dependency "libffi"
dependency "libyaml"
dependency "openssl"
dependency "zlib"

version("2.7.1") { source sha256: "d418483bdd0000576c1370571121a6eb24582116db0b7bb2005e90e250eae418" }
version("2.7.0") { source sha256: "8c99aa93b5e2f1bc8437d1bbbefd27b13e7694025331f77245d0c068ef1f8cbe" }

version("2.6.6") { source sha256: "364b143def360bac1b74eb56ed60b1a0dca6439b00157ae11ff77d5cd2e92291" }
version("2.6.5") { source sha256: "66976b716ecc1fd34f9b7c3c2b07bbd37631815377a2e3e85a5b194cfdcbed7d" }
version("2.6.4") { source sha256: "4fc1d8ba75505b3797020a6ffc85a8bcff6adc4dabae343b6572bf281ee17937" }
version("2.6.3") { source sha256: "577fd3795f22b8d91c1d4e6733637b0394d4082db659fccf224c774a2b1c82fb" }
version("2.6.2") { source sha256: "a0405d2bf2c2d2f332033b70dff354d224a864ab0edd462b7a413420453b49ab" }
version("2.6.1") { source sha256: "17024fb7bb203d9cf7a5a42c78ff6ce77140f9d083676044a7db67f1e5191cb8" }
version("2.6.0") { source sha256: "f3c35b924a11c88ff111f0956ded3cdc12c90c04b72b266ac61076d3697fc072" }

version("2.5.8") { source sha256: "6c0bdf07876c69811a9e7dc237c43d40b1cb6369f68e0e17953d7279b524ad9a" }

version("2.4.10") { source sha256: "93d06711795bfb76dbe7e765e82cdff3ddf9d82eff2a1f24dead9bb506eaf2d0" }
version("2.4.9") { source sha256: "f99b6b5e3aa53d579a49eb719dd0d3834d59124159a6d4351d1e039156b1c6ae" }
version("2.4.5") { source sha256: "6737741ae6ffa61174c8a3dcdd8ba92bc38827827ab1d7ea1ec78bc3cefc5198" }

source url: "https://cache.ruby-lang.org/pub/ruby/#{version.match(/^(\d+\.\d+)/)[0]}/ruby-#{version}.tar.gz"

relative_path "ruby-#{version}"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  env["CFLAGS"] << " -O3 -g -pipe"
  configure_command = [
    "--disable-dtrace",
    "--disable-install-doc",
    "--disable-install-rdoc",
    "--disable-jit-support",
    "--enable-shared",
    "--prefix=#{install_dir}",
    "--with-out-ext=dbm,readline",
    "--without-gdbm",
    "--without-gmp",
    "--without-valgrind",
    "--without-tk"
  ]
  configure(*configure_command, env: env)
  make "-j #{workers}", env: env
  make "-j #{workers} install", env: env
  command "#{install_dir}/bin/gem update --system"
  delete "#{install_dir}/embedded/share"
  delete "#{install_dir}/share"
  delete "#{install_dir}/lib/ruby/gems/**/cache"
  delete "#{install_dir}/lib/ruby/gems/**/doc"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/man"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/test"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/.travis.yml"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/.gitignore"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/bundler/man"
  delete "#{install_dir}/lib/ruby/gems/**/gems/**/.github"
  delete "#{install_dir}/lib/ruby/gems/**/gems/rubygems-update-*"
end
