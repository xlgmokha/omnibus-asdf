python_version = ENV.fetch('PYTHON_VERSION', '3.8.3')

name "python-#{python_version}"
maintainer "mkhan@gitlab.com"
homepage "https://gitlab.com/xlgmokha/omnibus-asdf"

install_dir "/opt/asdf/installs/python/#{python_version}"

build_version python_version
build_iteration 1

dependency "preparation"
override 'asdf_python', version: python_version
dependency "asdf_python"
