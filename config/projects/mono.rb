mono_version = ENV.fetch('MONO_VERSION', '6.8.0.123')

name "mono-#{mono_version}"
maintainer "mkhan@gitlab.com"
homepage "https://gitlab.com/xlgmokha/omnibus-asdf"

install_dir "/opt/asdf/installs/mono/#{mono_version}"

build_version mono_version
build_iteration 1

dependency "preparation"
override 'asdf_mono', version: mono_version
dependency "asdf_mono"
