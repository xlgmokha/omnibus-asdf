ruby_version = ENV.fetch('RUBY_VERSION', '2.7.1')

name "ruby-#{ruby_version}"
maintainer "mkhan@gitlab.com"
homepage "https://gitlab.com/xlgmokha/omnibus-asdf"

install_dir "/opt/asdf/installs/ruby/#{ruby_version}"

build_version ruby_version
build_iteration 1

dependency "preparation"
override 'asdf_ruby', version: ruby_version
dependency "asdf_ruby"

