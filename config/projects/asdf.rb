name "asdf"
maintainer "https://github.com/asdf-vm/asdf#contributing"
homepage "https://github.com/asdf-vm/asdf"

install_dir "/opt/asdf"

build_version '0.7.8'
build_iteration 1

dependency "asdf"
